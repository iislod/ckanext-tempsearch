======================================================
ckanext-tempsearch - Temporal search for CKAN datasets
======================================================

This extension provides a temporal search facet to search datasets by a specific date range.

.. image:: http://i.imgur.com/h7Ak3Qo.png

------------
Requirements
------------

This plugin is compatible with CKAN 2.3 or later.

------------
Installation
------------

With your virtualenv activated::

   cd src
   git clone https://gitlab.com/iislod/ckanext-tempsearch.git
   cd ckanext-tempsearch
   python setup.py develop

-------------
Configuration
-------------

1. Add the following field definition between ``<fields></fields>`` to your CKAN solr schema file (by default the schema file is located at ``/usr/lib/ckan/default/src/ckan/ckan/config/solr/schema.xml``)::

    <dynamicField name="point_time" type="date" indexed="true" stored="true" multiValued="true"/>

2. Index a custom field called "point_time" by implementing the `before_index`_ function in the ckan.plugins.interfaces.IPackageController class.

   (P.S. You can change the name "point_time" to any name you want. However, you should also change the "point_time" in ``ckanext/tempsearch/plugin.py`` accordingly.)

3. To add the temporal search widget to the sidebar of the search page, add this to the dataset search page template (``myproj/ckanext/myproj/templates/package/search.html``)::

   {% block secondary_content %}
   {% snippet "tempsearch/snippets/date_facet.html" %}
   {% endblock %}

4. Rebuild the search index. With your virtualenv activated::

    paster --plugin=ckan search-index rebuild -c /etc/ckan/default/production.ini

5. Add the following plugin to your CKAN config file (by default the config file is located at ``/etc/ckan/default/production.ini``), then restart your server::

    ckan.plugins = ... tempsearch

.. _before_index: http://docs.ckan.org/en/latest/extensions/plugin-interfaces.html?highlight=before_index#ckan.plugins.interfaces.IPackageController.before_index
