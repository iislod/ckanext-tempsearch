(function(ckan, document) {
  ckan.module('date-facet', function($, _) {
    return {
      options : {
        default_begin: null,
        default_end: null
      },
      initialize: function () {
        $.proxyAll(this, /_/);

        this.el.removeClass('js-hide');
        $('input', this.el).datepicker({
          dateFormat: 'yy-mm-dd'
        });

        var form = $(".search-form");
        $('<input type="hidden" />').attr({'id': 'ext_begin_date', 'name': 'ext_begin_date', 'value': this.options.default_begin}).appendTo(form);
        $('<input type="hidden" />').attr({'id': 'ext_end_date', 'name': 'ext_end_date', 'value': this.options.default_end}).appendTo(form);

        var defaultValues = {
          min: this._getDate(this.options.default_begin),
          max: this._getDate(this.options.default_end)
        };
        if (defaultValues.min === '' && defaultValues.max === '') {
	  $('[id="ext_begin_date"]').removeAttr('value');
	  $('[id="ext_end_date"]').removeAttr('value');
        }

        $('button', this.el).on('click', this._handleUpdateURL);
	$('.show-filters').click(this._checkForChanges);
      },
      _getDate: function (date) {
        if (date.length !== 0 && date !== true) {
          return new Date(date);
        }
        return '';
      },
      _handleUpdateURL: function (event) {
        var url = document.URL;
        url = this._updateQueryStringParameter(url, 'ext_begin_date', $('[name="begin"]', this.el).val());
        url = this._updateQueryStringParameter(url, 'ext_end_date', $('[name="end"]', this.el).val());
        window.location = url;
      },
      _updateQueryStringParameter: function (uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
          return uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
          return uri + separator + key + "=" + value;
        }
      }
    };
  });
}(window.ckan, document));
