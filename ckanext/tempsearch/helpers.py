import ckan.plugins as plugins


def get_date_url_param():
    params = ['', '']
    for k, v in plugins.toolkit.request.params.items():
        if k == 'ext_begin_date':
            params[0] = v
        elif k == 'ext_end_date':
            params[1] = v
        else:
            continue

    return params
